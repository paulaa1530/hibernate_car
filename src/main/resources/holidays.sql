    create table Travel (
       id integer not null auto_increment,
        adults integer not null,
        children integer not null,
        destination varchar(255),
        endDate date,
        source varchar(255),
        startDate date,
        primary key (id)
    ) engine=InnoDB;


create table Agency (
       name varchar(255) not null,
        commission integer not null,
        primary key (name)
    ) engine=InnoDB;


INSERT INTO Agency VALUES ("ITAKA", 200);
INSERT INTO Agency VALUES ("TUI", 250);
INSERT INTO Agency VALUES ("LIDL", 300);
INSERT INTO Agency VALUES ("NECKERMANN", 400);
INSERT INTO Agency VALUES ("ALMATUR", 100);