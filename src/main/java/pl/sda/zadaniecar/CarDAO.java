package pl.sda.zadaniecar;

import org.hibernate.Session;
import org.hibernate.Transaction;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public class CarDAO {
    private Session session;

    public CarDAO(Session session) {
        this.session = session;
    }

    public Car createCar(String brand, String model, CarBodyType carBodyType, LocalDate productionDate, String color, long km){
        Transaction transaction = session.beginTransaction();

        Car car = new Car(brand, model, carBodyType, productionDate, color, km);
        session.persist(car);

        transaction.commit();
        return car;
    }

    public Optional<Car> findCarById(int id){
        return session.byId(Car.class).loadOptional(id);
    }

    public Car updateCar(Car updatedCar){
        Transaction transaction = session.beginTransaction();

        Car car = session.find(Car.class, updatedCar.getId());
        car.setBrand(updatedCar.getBrand());
        car.setModel(updatedCar.getModel());
        car.setCarBodyType(updatedCar.getCarBodyType());
        car.setProductionDate(updatedCar.getProductionDate());
        car.setColor(updatedCar.getColor());
        car.setKm(updatedCar.getKm());

        transaction.commit();
        return car;
    }

    public Car deleteCar(int id){
        Transaction transaction = session.beginTransaction();

        Car car = session.find(Car.class, id);
        session.remove(car);

        transaction.commit();
        return car;
    }

    public List<Car> findAllCars(){
        return session.createQuery("from Car").getResultList();
    }

    public List<Car> findAllOrderByProductionDate(){
        return session.createQuery("from Car order by productionDate").getResultList();
    }



}
