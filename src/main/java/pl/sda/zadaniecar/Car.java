package pl.sda.zadaniecar;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;

@Entity
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String brand;

    private String model;

    @Enumerated(EnumType.STRING)
    private CarBodyType carBodyType;

    private LocalDate productionDate;

    @Transient
    private long age;

    private String color;

    private long km;

    private LocalDateTime modifiedDate;

    public Car(){

    }

    public Car(String brand, String model, CarBodyType carBodyType, LocalDate productionDate, String color, long km) {
        this.brand = brand;
        this.model = model;
        this.carBodyType = carBodyType;
        this.productionDate = productionDate;
        this.color = color;
        this.km = km;
    }

    @PrePersist
    @PreUpdate
    public void dateOfModification(){
        modifiedDate = LocalDateTime.now();
    }

    @PostLoad
    public void calculateAge(){
        System.out.println("Wykonuje się metoda 'calculateAge' w encji ");
        age = ChronoUnit.YEARS.between(productionDate, LocalDate.now());
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", carBodyType=" + carBodyType +
                ", productionDate=" + productionDate +
                ", age=" + age +
                ", color='" + color + '\'' +
                ", km=" + km +
                ", modifiedDate=" + modifiedDate +
                '}';
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public CarBodyType getCarBodyType() {
        return carBodyType;
    }

    public void setCarBodyType(CarBodyType carBodyType) {
        this.carBodyType = carBodyType;
    }

    public LocalDate getProductionDate() {
        return productionDate;
    }

    public void setProductionDate(LocalDate productionDate) {
        this.productionDate = productionDate;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public long getKm() {
        return km;
    }

    public void setKm(long km) {
        this.km = km;
    }


    public int getId() {
        return id;
    }
}
