package pl.sda.zadaniecar;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import pl.sda.hibernate.util.HibernateUtil;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class CarDAODemo {
    public static void main(String[] args) {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        Session session = sessionFactory.openSession();

        try{
            CarDAO carDAO = new CarDAO(session);
            //Car car1 = carDAO.createCar("Audi", "A6", CarBodyType.CABRIO, LocalDate.of(2015, 02, 06), "black", 0);
            //System.out.println("Car 1: "+car1);
            //carDAO.deleteCar(3);
            System.out.println("all cars: "+carDAO.findAllCars());

            Car car2 = carDAO.createCar("BMW", "M3", CarBodyType.SEDAN, LocalDate.of(2010, 05, 03), "red", 100L);

        }finally {
            session.close();
            sessionFactory.close();
        }
    }
}
