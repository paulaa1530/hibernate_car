package pl.sda.zadaniecar;

public enum CarBodyType {
    SEDAN,
    COMBI,
    CABRIO;
}
