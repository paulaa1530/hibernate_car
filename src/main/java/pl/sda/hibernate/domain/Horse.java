package pl.sda.hibernate.domain;

import javax.persistence.*;
import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.Date;

@Entity
@EntityListeners(LastUpdateListener.class)
public class Horse {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;  // niech nie bedzie w konstruktorze
	private String name;
	private LocalDate dateOfBirth;

	@Transient
	private long age;   // niech nie bedzie w konstruktorze
	private Date lastUpdate;

	public Horse() {
	}

	public Horse(String name, LocalDate dateOfBirth) {
		this.name = name;
		this.dateOfBirth = dateOfBirth;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public long getAge() {
		return age;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	@PostLoad
	public void calculateAge() {
		System.out.println("Wykonuje sie metoda 'calculateAge' w encji");
		age = ChronoUnit.YEARS.between(dateOfBirth, LocalDate.now());
	}

	@Override
	public String toString() {
		return "Horse{" +
				"id=" + id +
				", name='" + name + '\'' +
				", dateOfBirth=" + dateOfBirth +
				", age=" + age +
				", lastUpdate=" + lastUpdate +
				'}';
	}
}