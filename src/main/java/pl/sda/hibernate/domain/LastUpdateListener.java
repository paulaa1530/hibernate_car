package pl.sda.hibernate.domain;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.util.Date;

public class LastUpdateListener {

	@PreUpdate
	@PrePersist
	public void setLastUpdate(Horse c) {
		System.out.println("Wykonuje się metoda 'setLastUpdate' w Listenerze");
		c.setLastUpdate(new Date());
	}
}