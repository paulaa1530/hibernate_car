package pl.sda.hibernate.domain;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Entity
public class Membership {

	@EmbeddedId
	private MembershipPK id;

	private boolean gold;

	private boolean silver;

	private boolean active;

	public Membership() {
	}

	public Membership(MembershipPK id, boolean gold, boolean silver, boolean active) {
		this.id = id;
		this.gold = gold;
		this.silver = silver;
		this.active = active;
	}

	@Override
	public String toString() {
		return "Membership{" +
				"id=" + id +
				", gold=" + gold +
				", silver=" + silver +
				", active=" + active +
				'}';
	}
}
