package pl.sda.hibernate.domain;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class Home {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String name;

	@Embedded
	@AttributeOverrides({
			@AttributeOverride(name="country", column=@Column(name="COUNTRY")),
			@AttributeOverride(name="town", column=@Column(name="TOWN")),
			@AttributeOverride(name="street", column=@Column(name="STREET")),
	})
	private Address address;

	private LocalDate since;

	@Enumerated(EnumType.STRING)
	private MaterialType materialType;

	public Home() {
	}

	public Home(String name, Address address, LocalDate since, MaterialType materialType) {
		this.name = name;
		this.address = address;
		this.since = since;
		this.materialType = materialType;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public LocalDate getSince() {
		return since;
	}

	public void setSince(LocalDate since) {
		this.since = since;
	}

	public MaterialType getMaterialType() {
		return materialType;
	}

	public void setMaterialType(MaterialType materialType) {
		this.materialType = materialType;
	}

	@Override
	public String toString() {
		return "Home{" +
				"id=" + id +
				", name='" + name + '\'' +
				", address=" + address +
				", since=" + since +
				", materialType=" + materialType +
				'}';
	}
}
