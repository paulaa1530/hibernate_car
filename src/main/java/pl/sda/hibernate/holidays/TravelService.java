package pl.sda.hibernate.holidays;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import pl.sda.hibernate.util.HibernateUtil;

/**
 * Klasa uslugowa, wystawiona dla klientow.
 * Klasa korzysta z TravelDAO i AgencyDAO.
 * Przechowuje domyslne wartosci/mnozniki uzywane do wyliczania ceny za wycieczke.
 */
public class TravelService {

	/**
	 * Kwota za kilometr odleglosci wycieczki
	 */
	private static final int CHARGE_PER_KM = 10;
	/**
	 * Dodatkowa kwota doliczana za kazdy dzien pobytu doroslego
	 */
	private static final int ADULTS_EXTRA_CHARGE_PER_DAY = 20;
	/**
	 * Dodatkowa kwota doliczana za kazdy dzien pobytu maloletniego
	 */
	private static final int CHILDREN_EXTRA_CHARGE_PER_DAY = 50;
	/**
	 * Dodatkowa kwota doliczana, jesli wycieczka trwa (zahacza)
	 * w miesiacu lipcu, sierpniu lub grudniu
	 */
	private static final int SEASONAL_FEE = 100;

	private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
	private Session session = sessionFactory.openSession();


	private TravelDAO travelDAO = new TravelDAO(session);

	private AgencyDAO agencyDAO = new AgencyDAO(session);

	/**
	 * Metoda przygotowujaca oferte.
	 * Tworzy obiekt Travel na podstawie obiektu TravelParams i zapisuje go do bazy.
	 * Na jego podstawie oraz wybranej agencji wylicza kwote za wycieczke.
	 * Algorytm na kwote jest nastepujacy:
	 * - sumujemy:
	 *      a) kwote za dystans: wyliczana na podstawie pola distance (ilosc km * CHARGE_PER_KM)
	 *      b) dodatkowa kwote za kazdy dzien osoby doroslej i maloletniej
	 *      c) dodatkowa oplate sezonowa (jesli wycieczka zahacza o konkretne miesiace
	 *      d) prowizje agencji
	 */
	public Offer prepareOffer(TravelParams params) {

		// TODO:
		return null;
	}

	/**
	 * Metoda przygotowuje oferte dla wycieczki, ktora jest w bazie.
	 *
	 * @param travelId     - id wycieczki w bazie
	 * @param travelAgency - nazwa agencji
	 * @return
	 */
	public Offer calculateOffer(int travelId, String travelAgency) {

		// TODO:
		return null;
	}

	// TODO: udostepnij inne metody dla klienta dotyczace wycieczki
	// zastanow sie, jakie metody moga sie przydac klientowi (z TravelDAO)
}
