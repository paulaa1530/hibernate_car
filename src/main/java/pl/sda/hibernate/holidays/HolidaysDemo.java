package pl.sda.hibernate.holidays;

import pl.sda.hibernate.util.HibernateUtil;

/**
 * Punkt wejscia do systemu.
 * Tutaj wywolujemy wszystkie operacje.
 */
public class HolidaysDemo {

	public static void main(String[] args) {
		try {
			TravelService travelService = new TravelService();
			// tutaj uruchamiamy metode z travelService

		} finally {
			HibernateUtil.getSessionFactory().close();
		}
	}
}
