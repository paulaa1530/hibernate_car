package pl.sda.hibernate.holidays;

import javax.persistence.Entity;

/**
 * Encja reprezentujaca podroz.
 * Posiada:
 * id - identyfikator
 * source - miejsce startu
 * destination - miejsce docelowe
 * startDate - data poczatku wycieczki
 * endDate - data konca wycieczki
 * distance - kilometrowa odleglosc pomiedzy source i destination
 * na potrzeby naszego zadania wyliczamy ja mnozac przez 10
 * sume liter w source i destination :)
 * Wartosc ta mozna wyliczac w TravelDAO lub uzyc zdarzen zwrotnych w encji
 * Pole to NIE JEST zapisywane do bazy.
 * adults - ilosc osob doroslych
 * children - ilosc osob maloletnich (dzieci)
 */
@Entity
public class Travel {

	// TODO: zaimplementuj klase, zaprojektuj pola i metody
}
